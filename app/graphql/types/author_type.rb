Types::AuthorInputType = GraphQL::InputObjectType.define do
  name 'AuthorInputType'
  argument :first_name, types.String
  argument :last_name,  types.String
  argument :birth_year, types.Int
  argument :is_alive,   types.Boolean
end

Types::AuthorType =  GraphQL::ObjectType.define do
  name 'AuthorType'

  field :first_name, !types.String
  field :last_name, !types.String
  field :fullname, !types.String
  field :full_name, !types.String do
    description 'Author Fullname'
    resolve ->(obj, _, _) { "#{obj.first_name} #{obj.last_name}"}
  end
  field :birth_year, types.Int
  field :is_alive, types.Boolean
  field :id, !types.ID
  field :coordinates, Types::PairType do
    description 'Coor'
  end
  field :publication_years, types[types.Int]
  field :errors, types[types.String] do
    resolve ->(obj, _, _) { obj.errors.to_a }
  end
end
