Types::MutationType = GraphQL::ObjectType.define do
  name "Mutation"

  # field :createAuthor, Types::AuthorType do
  #   argument :first_name, types.String
  #   argument :last_name, types.String
  #   argument :birth_year, types.Int
  #   argument :is_alive, types.Boolean

  #   resolve ->(obj, args, ctx) { Author.create(args.to_h)}
  # end
  field :createAuthor, function: Mutations::CreateAuthor.new
  field :updateAuthor, function: Mutations::UpdateAuthor.new
  field :deleteAuthor, function: Mutations::DeleteAuthor.new

  field :logout, types.Boolean do
    resolve ->(_, _, ctx) { Session.where(key: ctx[:session_key]).destroy_all }
  end
end
