Types::QueryType = GraphQL::ObjectType.define do
  name "Query"

  field :login, types.String do
    is_public true
    argument :email, types.String
    argument :password, types.String
    resolve ->(_, args, _) {
      user = User.where(email: args[:email]).first
      user.sessions.create.key if user.try(:authenticate, args[:password])
    }
  end

  field :current_user, Types::UserType do
    resolve ->(_, _, ctx) { ctx[:current_user] }
  end

  field :author, Types::AuthorType do
    must_be [:superadmin]
    argument :id, types.ID
    description "Single Author"
    resolve ->(obj, args, ctx) {
      Author.where(id: args[:id]).first
    }
  end

  # Open to Public Query
  field :authors, types[Types::AuthorType] do
    is_public true
    resolve ->(_, _, _) { Author.all }
  end
end
