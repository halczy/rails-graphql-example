Types::PairType = GraphQL::ObjectType.define do
  name 'PairType'

  field :latitude, types.Float do
    resolve ->(obj, _, _) { obj.first }
  end

  field :longitude, types.Float do
    resolve ->(obj, _, _) { obj.last }
  end
end
