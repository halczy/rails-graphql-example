Types::UserType = GraphQL::ObjectType.define do
  name "UserType"

  field :email, !types.String
  field :id,    types.ID
  field :is_superadmin, types.Boolean
end
