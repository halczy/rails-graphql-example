class User < ApplicationRecord
  has_many :sessions

  has_secure_password

  def role
    return :superadmin if is_superadmin?
  end
end
