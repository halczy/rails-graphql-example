class Author < ApplicationRecord

  validates :last_name, presence: true

  def fullname
    "#{first_name} #{last_name}"
  end

  def coordinates
    [rand(50), rand(90)]
  end

  def publication_years
    (1..rand(50)).to_a
  end
end
